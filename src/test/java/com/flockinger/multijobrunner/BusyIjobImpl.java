package com.flockinger.multijobrunner;

public class BusyIjobImpl implements IJob{

	private JobDoneChecker doneChecker;
	
	public BusyIjobImpl(JobDoneChecker doneChecker) {
		this.doneChecker = doneChecker;
	}
	
	@Override
	public void execute() throws Exception {
			Thread.sleep(1000);
			doneChecker.setDone();
	}
}
