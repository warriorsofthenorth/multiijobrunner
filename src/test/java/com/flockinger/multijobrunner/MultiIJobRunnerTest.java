package com.flockinger.multijobrunner;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

public class MultiIJobRunnerTest {

	private MultiIJobRunner multiIJobRunner;
	
	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void initializeWithNullAsJobList_shouldStillRun() throws Exception {
		multiIJobRunner = new MultiIJobRunner(1, null);
		multiIJobRunner.execute();
	}
	
	@Test
	public void initializeWithNegativeThreadPoolCount_shouldStillRun() throws Exception {
		List<DoneCheckerAndIJob> jobsAndChecker = getBusyJobs(2);
		List<JobDoneChecker> checker = getCheckersfrom(jobsAndChecker);
		List<IJob> jobs = getJobsfrom(jobsAndChecker);
		
		multiIJobRunner = new MultiIJobRunner(-3, jobs);
		multiIJobRunner.execute();
		
		assertTrue(checker.get(0).isDone());
		assertTrue(checker.get(1).isDone());
	}
	
	
	@Test
	public void correctAmountOfInstances_shouldCompleteAllJobs() throws Exception {
		List<DoneCheckerAndIJob> jobsAndChecker = getBusyJobs(2);
		List<JobDoneChecker> checker = getCheckersfrom(jobsAndChecker);
		List<IJob> jobs = getJobsfrom(jobsAndChecker);
		
		multiIJobRunner = new MultiIJobRunner(2, jobs);
		multiIJobRunner.execute();
		
		assertTrue(checker.get(0).isDone());
		assertTrue(checker.get(1).isDone());
	}
	
	@Test
	public void tooMuchInstancesAtTheSameTime_shouldStillAndCompleteAllJobs() throws Exception {
		List<DoneCheckerAndIJob> jobsAndChecker = getBusyJobs(4);
		List<JobDoneChecker> checker = getCheckersfrom(jobsAndChecker);
		List<IJob> jobs = getJobsfrom(jobsAndChecker);
		
		multiIJobRunner = new MultiIJobRunner(2, jobs);
		multiIJobRunner.execute();
		
		assertTrue(checker.get(0).isDone());
		assertTrue(checker.get(1).isDone());
		assertTrue(checker.get(2).isDone());
		assertTrue(checker.get(3).isDone());
	}

	@Test(expected=ArithmeticException.class)
	public void runWithFaultyJob_ShouldThrowExceptionOutside() throws Exception {
		List<DoneCheckerAndIJob> jobsAndChecker = getBusyJobs(4);
		List<IJob> jobs = getJobsfrom(jobsAndChecker);
		jobs.add(new DangerousIjobImpl());
		
		multiIJobRunner = new MultiIJobRunner(5, jobs);
		multiIJobRunner.execute();
	}
	
	private List<JobDoneChecker> getCheckersfrom(List<DoneCheckerAndIJob> jobsAndChecker)
	{
		List<JobDoneChecker> checker = jobsAndChecker.stream()
				.map( (jobNCheck) -> jobNCheck.getJobDoneChecker() )
				.collect( Collectors.toCollection(ArrayList::new) );
		return checker;
	}
	
	private List<IJob> getJobsfrom(List<DoneCheckerAndIJob> jobsAndChecker)
	{
		List<IJob> jobs = jobsAndChecker.stream()
				.map( (jobNCheck) -> jobNCheck.getiJob() )
				.collect( Collectors.toCollection(ArrayList::new) );
		return jobs;
	}
	
	private List<DoneCheckerAndIJob> getBusyJobs(int amount)
	{
		List<DoneCheckerAndIJob> jobs = new ArrayList<>();
		JobDoneChecker doneChecker;
		
		for(int jobCount=0; jobCount < amount; jobCount++){
			doneChecker = new JobDoneChecker();
			jobs.add( new DoneCheckerAndIJob().setiJob(new BusyIjobImpl(doneChecker))
											  .setJobDoneChecker(doneChecker) );	
		}
		
		return jobs;
	}
}
