package com.flockinger.multijobrunner;

public class DoneCheckerAndIJob {

	private JobDoneChecker jobDoneChecker;
	private IJob iJob;
	
	
	public JobDoneChecker getJobDoneChecker() {
		return jobDoneChecker;
	}
	
	public DoneCheckerAndIJob setJobDoneChecker(JobDoneChecker jobDoneChecker) {
		this.jobDoneChecker = jobDoneChecker;
		return this;
	}
	
	public IJob getiJob() {
		return iJob;
	}
	
	public DoneCheckerAndIJob setiJob(IJob iJob) {
		this.iJob = iJob;
		return this;
	}
	
	
}
