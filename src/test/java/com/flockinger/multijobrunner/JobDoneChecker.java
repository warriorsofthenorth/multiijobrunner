package com.flockinger.multijobrunner;

public class JobDoneChecker {

	private boolean isDone = false;
	
	public void setDone(){
		isDone = true;
	}
	
	public boolean isDone(){
		return isDone;
	}
}
