package com.flockinger.multijobrunner;
import java.util.ArrayList;
import java.util.List;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.JobListener;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.UnableToInterruptJobException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class IJobListener implements JobListener {

	private Logger logger = LoggerFactory.getLogger(this.getClass());
	private Exception iJobException = null;

	@Override
	public String getName() {
		return this.getClass().getName();
	}

	@Override
	public void jobToBeExecuted(JobExecutionContext context) {
	}

	@Override
	public void jobExecutionVetoed(JobExecutionContext context) {
	}

	@Override
	public void jobWasExecuted(JobExecutionContext context, JobExecutionException jobException) {
		logger.debug("listening to jobresult", jobException);
		if (isAJobExceptionThrown(jobException)){
			stopAllRunningThreads(context.getScheduler());
			try {
				context.getScheduler().shutdown();
			} catch (SchedulerException e) {
				logger.debug("",e);
			}
			iJobException = jobException;
		}
	}
	
	public Exception getPossibleJobException()
	{
		return iJobException;
	}

	private boolean isAJobExceptionThrown(JobExecutionException jobException) {
		return jobException != null && jobException.getUnderlyingException() instanceof Exception;
	}
	
	public void stopAllRunningThreads(Scheduler scheduler) {
		for (JobExecutionContext runningContext : getRunningContexts(scheduler)) {
			interruptJob(scheduler, runningContext);
		}
	}

	private List<JobExecutionContext> getRunningContexts(Scheduler scheduler)
	{
		List<JobExecutionContext> runningContexts = new ArrayList<>();
		
		try {
			runningContexts = scheduler.getCurrentlyExecutingJobs();
		} catch (SchedulerException schedulerException) {
			logger.debug("Errow while getting currently executing jobs", schedulerException);
		}
		
		return runningContexts;
	}
	
	private void interruptJob(Scheduler scheduler, JobExecutionContext runningContext)
	{
		try {
			scheduler.interrupt(runningContext.getJobDetail().getKey());
		} catch (UnableToInterruptJobException uninterrupableException) {
			logger.debug("Add InterruptableJob interface to job class" , uninterrupableException);
		}
	}
}
