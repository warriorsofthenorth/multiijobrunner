package com.flockinger.multijobrunner;
import java.util.Date;

public class JobAndTime {
	private IJob iJob;
	private Date time;
	
	public IJob getiJob() {
		return iJob;
	}
	public JobAndTime setiJob(IJob iJob) {
		this.iJob = iJob;
		return this;
	}
	public Date getTime() {
		return time;
	}
	public JobAndTime setTime(Date time) {
		this.time = time;
		return this;
	}
}
