package com.flockinger.multijobrunner;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.apache.log4j.BasicConfigurator;
import org.quartz.JobBuilder;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SchedulerFactory;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MultiIJobRunner implements IJob {

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	private Scheduler scheduler;
	private IJobListener iJobListener;
	private List<IJob> scheduledJobs;
	public final static String JOB_MAPNAME = "JOB";

	public MultiIJobRunner(int poolSize, List<IJob> scheduledJobs) throws SchedulerException {
		BasicConfigurator.configure();
		this.scheduledJobs =  new ArrayList<>();
		iJobListener = new IJobListener();
		
		if(poolSize < 1)
			poolSize = 1;
		
		if(scheduledJobs != null)
			this.scheduledJobs = scheduledJobs;
		
		init(poolSize);
	}

	private void init(int poolSize) throws SchedulerException {
		Properties props = new Properties();
		props.setProperty("org.quartz.threadPool.threadCount", String.valueOf(poolSize));
		props.setProperty("org.quartz.scheduler.skipUpdateCheck", "true");
		SchedulerFactory schedulerFactory = new StdSchedulerFactory(props);
		scheduler = schedulerFactory.getScheduler();
		scheduler.getListenerManager().addJobListener(iJobListener);
	}

	public void execute() throws Exception {
		try {
			scheduler.start();
			addJobs();
			Thread.sleep(2000);
			scheduler.shutdown(true);
			
			if(iJobListener.getPossibleJobException() != null)
				throw iJobListener.getPossibleJobException();
		
		} catch (SchedulerException exception) {
			logger.debug("Scheduler job creation", exception);

			throw getUnderlyingException(exception);
		}
	}
	
	private void addJobs() throws SchedulerException
	{
		for (IJob scheduledJob : scheduledJobs) {
			scheduler.scheduleJob(getJobDetail(scheduledJob), getTrigger());
		}
	}
	

	private Exception getUnderlyingException(SchedulerException exception) {
		Throwable underlyingThrowable = exception.getUnderlyingException();
		Exception underlyingException = exception;

		if (underlyingThrowable instanceof Exception)
			underlyingException = (Exception) underlyingThrowable;

		return underlyingException;
	}

	private JobDetail getJobDetail(IJob job) {
		JobDataMap jobDataMap = new JobDataMap();
		jobDataMap.put(JOB_MAPNAME, job);
		JobDetail jobDetail = JobBuilder.newJob(IJobRunnable.class)
										.usingJobData(jobDataMap)
										.withIdentity(JobKey.createUniqueName("IJOB"))
										.build();
		return jobDetail;
	}

	private Trigger getTrigger() {
		Trigger trigger = TriggerBuilder.newTrigger()
										.startNow()
										.build();
		return trigger;
	}
}
