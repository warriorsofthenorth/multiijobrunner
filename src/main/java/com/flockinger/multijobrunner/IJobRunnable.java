package com.flockinger.multijobrunner;
import org.quartz.InterruptableJob;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.UnableToInterruptJobException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class IJobRunnable implements InterruptableJob {

	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {

		logger.debug("in execute of ijob");
		IJob iJob = null;
		Object objJob = context.getMergedJobDataMap().get(MultiIJobRunner.JOB_MAPNAME);

		if (objJob != null && objJob instanceof IJob)
			iJob = (IJob) objJob;

		try {
			if (iJob != null)
				iJob.execute();
		} catch (Exception e) {
			JobExecutionException jobEx = new JobExecutionException(e);
			jobEx.unscheduleAllTriggers();	
			throw jobEx;
		}
	}

	@Override
	public void interrupt() throws UnableToInterruptJobException {
	}
}
