package com.flockinger.multijobrunner;

public interface IJob {
	public void execute() throws Exception;
}
